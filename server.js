var express      = require('express');

var app = express()
  .use(express.static(__dirname + '/dist'))
  .listen(process.env.PORT || 5000, function() {
    console.log('Listening on port %d', app.address().port);
  });

